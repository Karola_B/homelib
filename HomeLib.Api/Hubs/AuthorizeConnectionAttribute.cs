﻿using System;
using System.Linq;
using HomeLib.Services;
using HomeLibrary.Data;
using HomeLibrary.Data.Repositories;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace HomeLib.Api.Hubs
{
    [AttributeUsage(AttributeTargets.Method, Inherited = false)]
    public class AuthorizeConnectionAttribute : AuthorizeAttribute
    {
        public IAuthenticationService AuthenticationService { get; set; }
        private const int UserNameParameterIndex = 0;
        private string[] AnonymousMethods { get; } = { "Login" };

        public bool AllowAnonymous { get; set; }

        public override bool AuthorizeHubMethodInvocation(IHubIncomingInvokerContext hubIncomingInvokerContext, bool appliesToMethod)
        {
            var connectionId = hubIncomingInvokerContext.Hub.Context.ConnectionId;

            return AnonymousAccessAllowed(hubIncomingInvokerContext) || IsAllowedToGetData(connectionId);
        }

        private bool AnonymousAccessAllowed(IHubIncomingInvokerContext hubIncomingInvokerContext)
        {
            return AnonymousMethods.Any(x => x == hubIncomingInvokerContext.MethodDescriptor.Name);
        }

        private bool IsAllowedToGetData(string connectionId)
        {
            var authenticationService = new AuthenticationService(new UsersRepository(new LibraryContext()));

            var connectionForTeam = authenticationService.ValidateConnection(connectionId);

            if (connectionForTeam == null)
            {
                return false;
            }

            return true;
        }
    }
}