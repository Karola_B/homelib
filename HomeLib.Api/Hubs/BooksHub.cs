using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HomeLib.Api.Requests;
using HomeLib.Api.Responses;
using HomeLib.Services;
using HomeLibrary.Data.Models;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace HomeLib.Api.Hubs
{
    [HubName("library")]
    public class BooksHub : Hub
    {
        private readonly IBookService _bookService;
        private readonly IValidationService _validationService;
        private readonly IUsersService _usersService;

        public BooksHub(IBookService bookService, IValidationService validationService, IUsersService usersService)
        {
            _bookService = bookService;
            _validationService = validationService;
            _usersService = usersService;
        }

        [AuthorizeConnection]
        public void GetLibraryState()
        {
            var books = _bookService.GetBooksForLocalisation(LocalisationEnum.All);
            var bookDtos = MapToBookDto(books);

            var isClientRegistered = _usersService.GetByConnection(Context.ConnectionId) != null;
            Clients.Caller.libraryStateReceived(new LibraryStateResponse { Books = bookDtos.GroupBy(x => x.ISBN), Borrowers = new List<BorrowerDto>(), Registered = isClientRegistered });
        }

        [AuthorizeConnection]
        public GetBooksResponse GetBooks(GetBooksRequest request)
        {
            var books = _bookService.GetBooksForLocalisation(request.Localisation);

            var bookDtos = MapToBookDto(books);
            var getBooksResponse = new GetBooksResponse { Books = bookDtos.GroupBy(x => x.ISBN) };

            Clients.Caller.booksUpdate(getBooksResponse);
            return getBooksResponse;
        }

        [AuthorizeConnection]
        public FindBooksResponse FindBooks(FindBooksRequest request)
        {
            if (!_validationService.ValidateIsbn(request.ISBN))
            {
                Clients.Caller.invalidIsbn(new InvalidIsbnResponse { ISBN = request.ISBN });
            }

            var books = _bookService.FindBooks(request.ISBN);

            if (!books.Any())
            {
                var bookNotFoundResponse = new FindBooksResponse { BookExists = false, Books = new[] { new BookDto { ISBN = request.ISBN } } };
                Clients.Caller.bookNotFound(bookNotFoundResponse);
                return bookNotFoundResponse;
            }

            var bookFoundResponse = new FindBooksResponse { BookExists = true, Books = books.ToArray() };
            Clients.Caller.bookFound(bookFoundResponse);

            return bookFoundResponse;
        }

        [AuthorizeConnection]
        public void UpdateBook(UpdateBookRequest request)
        {
            _bookService.UpdateBook(request.Book);

            Clients.Caller.bookUpdated(new BookUpdateResponse { UpdatedBook = request.Book });
        }

        [AuthorizeConnection]
        public RegisterBookResponse RegisterBook(RegisterBookRequest request)
        {
            var registeredBookId = _bookService.RegisterBook(request.Book);

            if (registeredBookId > -1)
            {
                var bookRegisteredResponse = new RegisterBookResponse { Registered = true, Book = request.Book };
                Clients.All.bookRegistered(bookRegisteredResponse);
                return bookRegisteredResponse;
            }

            var bookNotRegisteredResponse = new RegisterBookResponse { Registered = false, Book = request.Book };
            Clients.Caller.errorDuringRegistration(bookNotRegisteredResponse);
            return bookNotRegisteredResponse;
        }

        public void Login(LoginRequest request)
        {
            if (string.IsNullOrEmpty(request.FacebookId) || string.IsNullOrEmpty(request.UserName))
            {
                Clients.Caller.userNotRegistered();
                return;
            }

            var registeredUser = _usersService.RegisterUser(request.FacebookId, request.UserName, Context.ConnectionId);
            if (registeredUser == null)
            {
                Clients.Caller.userNotRegistered();
            }
            else
            {
                Clients.Caller.userRegistered(new RegisterResponse { Login = registeredUser.Login });
            }

            // TODO: notify all active Maurices about new user
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            _usersService.LogoutUserWithConnectionId(Context.ConnectionId);
            return base.OnDisconnected(stopCalled);
        }

        public void Logout(LogoutRequest request)
        {
            _usersService.LogoutUserWithFacebookId(request.FacebookId);
            Clients.Caller.logout();
        }

        private static BookDto[] MapToBookDto(IList<Book> books)
        {
            return books.Select(x =>
                new BookDto
                {
                    BookId = x.BookId,
                    Author = x.Author,
                    Title = x.Title,
                    ISBN = x.ISBN,
                    Description = x.Description,
                    Publisher = x.Publisher,
                    PublishDate = x.PublishDate,
                    Cover = x.Cover,
                    Localisation = x.Localisation.ToString(),
                    Condition = x.Condition.ToString()
                }).ToArray();
        }
    }
}