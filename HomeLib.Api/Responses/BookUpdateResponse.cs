﻿using HomeLib.Services;

namespace HomeLib.Api.Responses
{
    public class BookUpdateResponse
    {
        public BookDto UpdatedBook { get; set; }
    }
}