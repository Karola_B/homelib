﻿using System.Collections.Generic;
using System.Linq;
using HomeLib.Services;

namespace HomeLib.Api.Responses
{
    public class GetBooksResponse
    {
        public IEnumerable<IGrouping<string, BookDto>> Books { get; set; }
    }
}