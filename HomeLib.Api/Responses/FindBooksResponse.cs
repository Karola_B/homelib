﻿using System.Collections.Generic;
using System.Linq;
using HomeLib.Services;

namespace HomeLib.Api.Responses
{
    public class FindBooksResponse
    {
        public BookDto Book { get; set; }
        public bool BookExists { get; set; }
        public BookDto[] Books { get; set; }
    }
}