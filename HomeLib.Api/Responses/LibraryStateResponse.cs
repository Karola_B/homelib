﻿using System.Collections.Generic;
using System.Linq;
using HomeLib.Services;

namespace HomeLib.Api.Responses
{
    public class LibraryStateResponse
    {
        public IEnumerable<IGrouping<string, BookDto>> Books { get; set; }
        public List<BorrowerDto> Borrowers { get; set; }
        public bool Registered { get; set; }
    }
}