﻿using HomeLib.Services;

namespace HomeLib.Api.Responses
{
    public class RegisterBookResponse
    {
        public BookDto Book { get; set; }
        public bool Registered { get; set; }
    }
}