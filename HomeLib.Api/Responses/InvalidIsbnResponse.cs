namespace HomeLib.Api.Responses
{
    public class InvalidIsbnResponse
    {
        public string ISBN { get; set; }
    }
}