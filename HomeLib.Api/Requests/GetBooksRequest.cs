﻿using HomeLibrary.Data.Models;

namespace HomeLib.Api.Requests
{
    public class GetBooksRequest
    {
        public LocalisationEnum Localisation { get; set; }
    }
}