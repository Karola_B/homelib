﻿namespace HomeLib.Api.Requests
{
    public class LogoutRequest
    {
        public string FacebookId { get; set; }
    }
}