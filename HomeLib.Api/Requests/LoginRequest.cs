﻿namespace HomeLib.Api.Requests
{
    public class LoginRequest
    {
        public string FacebookId { get; set; }
        public string UserName { get; set; }
    }
}