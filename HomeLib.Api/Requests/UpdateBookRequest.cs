﻿using HomeLib.Services;

namespace HomeLib.Api.Requests
{
    public class UpdateBookRequest
    {
        public BookDto Book { get; set; }
    }
}