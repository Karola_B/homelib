﻿using HomeLib.Services;

namespace HomeLib.Api.Requests
{
    public class RegisterBookRequest
    {
        public BookDto Book;
    }
}