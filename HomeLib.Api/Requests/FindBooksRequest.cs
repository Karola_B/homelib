﻿namespace HomeLib.Api.Requests
{
    public class FindBooksRequest
    {
        public string ISBN { get; set; }
    }
}