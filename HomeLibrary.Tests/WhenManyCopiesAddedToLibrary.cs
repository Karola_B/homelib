using System.Collections.Generic;
using HomeLib.BooksInformationService;
using HomeLib.Services;
using HomeLibrary.Data.Models;
using HomeLibrary.Data.Repositories;
using Moq;
using NUnit.Framework;

namespace HomeLibrary.Tests
{
    public class WhenManyCopiesAddedToLibrary
    {
        private Mock<IBooksInformationService> _bookInformationServiceMock;
        private Mock<IBooksRepository> _bookRepositoryMock;
        private string _isbn;
        private BookService _bookService;

        [SetUp]
        public void Initailize()
        {
            _bookInformationServiceMock = new Mock<IBooksInformationService>();
            _bookRepositoryMock = new Mock<IBooksRepository>();

            _isbn = "9788378397649";

            _bookRepositoryMock.Setup(x => x.GetBooksForLocalisation(LocalisationEnum.All))
                .Returns(new List<Book>
                {
                    new Book()
                    {
                        BookId = 1,
                        ISBN = _isbn,
                        Localisation = LocalisationEnum.Gliwice
                    },
                    new Book()
                    {
                        BookId = 2,
                        ISBN = _isbn,
                        Localisation = LocalisationEnum.Imielin
                    }
                });

            _bookService = new BookService(new[] { _bookInformationServiceMock.Object }, _bookRepositoryMock.Object);
        }

        [Test]
        public void ShouldReturnAllCopiesWhenGetBooksForLocalisationCalled()
        {
            var books = _bookService.GetBooksForLocalisation(LocalisationEnum.All);

            Assert.That(books.Count, Is.EqualTo(2));
        }
    }
}