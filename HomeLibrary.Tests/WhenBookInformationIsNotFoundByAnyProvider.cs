﻿using System.Collections.Generic;
using HomeLib.BooksInformationService;
using HomeLib.Services;
using HomeLibrary.Data.Models;
using HomeLibrary.Data.Repositories;
using Moq;
using NUnit.Framework;

namespace HomeLibrary.Tests
{
    public class WhenBookInformationIsNotFoundByAnyProvider
    {
        private Mock<IBooksInformationService> _bookInformationServiceMock;
        private string _isbn;
        private Mock<IBooksRepository> _bookRepositoryMock;
        private IList<BookDto> _books;

        [SetUp]
        public void Setup()
        {
            _bookInformationServiceMock = new Mock<IBooksInformationService>();
            _bookRepositoryMock = new Mock<IBooksRepository>();

            _isbn = "9788378397649";

            _bookInformationServiceMock.Setup(x => x.GetByIsbn(_isbn))
                .Returns(() => null);

            var bookService = new BookService(new []{ _bookInformationServiceMock.Object }, _bookRepositoryMock.Object);
            _books = bookService.FindBooks(_isbn);
        }

        [Test]
        public void ShouldReturnEmptyList()
        {
            Assert.That(_books, Is.Empty);
        }
    }
}