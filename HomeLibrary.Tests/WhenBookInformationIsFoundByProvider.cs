﻿using System.Linq;
using HomeLib.BooksInformationService;
using HomeLib.Services;
using HomeLibrary.Data.Models;
using HomeLibrary.Data.Repositories;
using Moq;
using NUnit.Framework;

namespace HomeLibrary.Tests
{
    public class WhenBookInformationIsFoundByProvider
    {
        private Mock<IBooksInformationService> _bookInformationServiceMock;
        private Mock<IBooksRepository> _bookRepositoryMock;
        private string _isbn;
        private BookDto _book;

        [SetUp]
        public void Initailize()
        {
            _bookInformationServiceMock = new Mock<IBooksInformationService>();
            _bookRepositoryMock = new Mock<IBooksRepository>();

            _isbn = "9788378397649";

            _bookInformationServiceMock.Setup(x => x.GetByIsbn(_isbn))
                .Returns(new BookInformation()
                {
                    ISBN = _isbn
                });

            var bookService = new BookService(new[] { _bookInformationServiceMock.Object }, _bookRepositoryMock.Object);
            _book = bookService.FindBooks(_isbn).First();
        }

        [Test]
        public void ShouldReturnBookInformationWithGivenIsbn()
        {
            Assert.That(_book.ISBN, Is.EqualTo(_isbn));
        }
    }
}