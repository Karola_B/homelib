﻿using System.Collections.Generic;
using System.Linq;
using HomeLib.BooksInformationService;
using HomeLib.Services;
using HomeLibrary.Data.Models;
using HomeLibrary.Data.Repositories;
using Moq;
using NUnit.Framework;

namespace HomeLibrary.Tests
{
    public class WhenBookExistsInLibrary
    {
        private Mock<IBooksInformationService> _bookInformationServiceMock;
        private Mock<IBooksRepository> _bookRepositoryMock;
        private string _isbn;
        private BookService _bookService;

        [SetUp]
        public void Initailize()
        {
            _bookInformationServiceMock = new Mock<IBooksInformationService>();
            _bookRepositoryMock = new Mock<IBooksRepository>();

            _isbn = "9788378397649";

            _bookRepositoryMock.Setup(x => x.GetBooksByIsbn(_isbn))
                .Returns(new List<Book>
                {
                    new Book
                    {
                        BookId = 1,
                        ISBN = _isbn
                    }
                }
            );

            _bookRepositoryMock.Setup(x => x.AddBookToLibrary(It.IsAny<Book>()))
                .Returns(1);

            _bookService = new BookService(new[] { _bookInformationServiceMock.Object }, _bookRepositoryMock.Object);
        }

        [Test]
        public void ShouldReturnExistingBookWhenFindBookCalled()
        {
            var book = _bookService.FindBooks(_isbn).First();

            Assert.That(book.ISBN, Is.EqualTo(_isbn));
            Assert.That(book.BookId, Is.EqualTo(1));
        }

        [Test]
        public void ShouldBePossibleAddingAnotherCopy()
        {
            var bookCopy = new BookDto()
            {
                ISBN = _isbn
            };

            var copyId = _bookService.RegisterBook(bookCopy);

            Assert.That(copyId, Is.GreaterThan(0));
        }
    }
}