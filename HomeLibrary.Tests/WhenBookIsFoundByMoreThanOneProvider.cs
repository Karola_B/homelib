﻿using System.Collections.Generic;
using System.Linq;
using HomeLib.BooksInformationService;
using HomeLib.Services;
using HomeLibrary.Data.Models;
using HomeLibrary.Data.Repositories;
using NUnit.Framework;
using Moq;

namespace HomeLibrary.Tests
{
    public class WhenBookIsFoundByMoreThanOneProvider
    {
        private Mock<IBooksInformationService> _goodBookInformationServiceMock;
        private Mock<IBooksRepository> _bookRepositoryMock;
        private string _isbn;
        private IList<BookDto> _books;
        private Mock<IBooksInformationService> _badBookInformationServiceMock;

        [SetUp]
        public void Initailize()
        {
            _goodBookInformationServiceMock = new Mock<IBooksInformationService>();
            _badBookInformationServiceMock = new Mock<IBooksInformationService>();
            _bookRepositoryMock = new Mock<IBooksRepository>();

            _isbn = "9788378397649";

            _badBookInformationServiceMock.Setup(x => x.GetByIsbn(_isbn))
                .Returns(new BookInformation()
                {
                    Title = "Bad title",
                    ISBN = _isbn
                });

            _goodBookInformationServiceMock.Setup(x => x.GetByIsbn(_isbn))
                .Returns(new BookInformation()
                {
                    Title = "Good title",
                    ISBN = _isbn
                });

            var bookService = new BookService(new[] { _goodBookInformationServiceMock.Object, _badBookInformationServiceMock.Object }, _bookRepositoryMock.Object);
            _books = bookService.FindBooks(_isbn);
        }

        [Test]
        public void ShouldReturnListOfAllFoundBookInformationItems()
        {
            Assert.That(_books.Any(x => x.Title == "Good title"));
            Assert.That(_books.Any(x => x.Title == "Bad title"));
        }
    }
}
