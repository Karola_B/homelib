/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  TextInput,
  View,
  TouchableHighlight,
  ToastAndroid,
  TouchableOpacity,
  Image,
  ScrollView,
  RefreshControl,
  BackAndroid,
  DrawerLayoutAndroid,
} from 'react-native';

import BarcodeScanner from 'react-native-barcodescanner';
import signalr from 'react-native-signalr';
import { COLOR, ThemeProvider, ActionButton, Drawer, Toolbar,Button, ListItem, Avatar } from 'react-native-material-ui';
import ScanView from './Application/ScanView'
import Book from './Application/Book'
import NewBook from './Application/NewBook'
import SelectBookToRegister from './Application/SelectBookToRegister'
import BookList from './Application/BookList'
import BookCopies from './Application/BookCopies'
import BookListItem from './Application/BookListItem'
import LocalisationAvatar from './Application/LocalisationAvatar'
import InsertIsbn from './Application/InsertIsbn'
import DeviceInfo from 'react-native-device-info';

export default class HomeLibrary extends Component {
  constructor(props) {
    super(props);
    var proxy;
    var connection;
    var serverAddress;
    this.state = {
      book: null,
      newBook: null,
      signalrConnected: false,
      refreshing: false,
      currentView: 'Books',
      currentBook: null,
      books: [],
      selectedCopies: [],
      navigateFromCopies: false
    };
  }

  componentDidMount() {
        //serverAddress = 'http://fp-pc2249:8088/';
        serverAddress = 'http://192.168.0.19:8088/signalr/';
        //serverAddress = 'https://zyrafy.azurewebsites.net/'
        connection = signalr.hubConnection(serverAddress);
        connection.logging = true;
        proxy = connection.createHubProxy('library');
        proxy.on('invalidIsbn', (isbn) => {
            ToastAndroid.show('Invalid isbn provided: ' + isbn, ToastAndroid.SHORT);
        });

        proxy.on('bookRegistered', (response) => {
            ToastAndroid.show('book registered: ' + response.Book.Title, ToastAndroid.SHORT);
            proxy.invoke('getBooks',{Localisation: 'All'});
        });

        proxy.on('errorDuringRegistration', (book) => {
            ToastAndroid.show('Error during book registration: ' + book.Title, ToastAndroid.SHORT);
        });

        proxy.on('bookNotFound', (response) => {
            this.setState({ 
              currentView: 'RegisterBook',
              canProcessBarcode: true,
              navigateFromFoundBooks: false,
              newBook: response.Books[0]
            });
            ToastAndroid.show('Book not found: ' + response.Books[0].ISBN, ToastAndroid.SHORT);
        });

        proxy.on('bookFound', (response) => {
          if (response.Books.length === 1){
            this.setState({ 
              newBook: response.Books[0],
              currentView: 'RegisterBook',
              canProcessBarcode: true
            });
          } else {
            this.setState({ 
              newBooks: response.Books,
              currentView: 'SelectBookToRegister',
              canProcessBarcode: true
            });
          }
        });

        proxy.on('booksUpdate', (response) => {
          var totalBooks = 0;
          response.Books.map((books) => {return books.length;}).forEach(function(x){totalBooks += parseInt(x,10)})
          ToastAndroid.show(totalBooks + ' book(s) received', ToastAndroid.SHORT);
          
          this.setState({ books: response.Books, signalrConnected: true, canProcessBarcode: true })
        });

        proxy.on('bookUpdated', (response) => {
          proxy.invoke('getBooks',{Localisation: 'All'});
          this.setState({ currentBook: response.UpdatedBook })
        });

        // atempt connection, and handle errors
        connection.start().done(() => { 
            ToastAndroid.show('signalr connected', ToastAndroid.SHORT);
            proxy.invoke('login',{FacebookId: DeviceInfo.getUniqueID(), UserName: 'Maurice' })
              .done(() => {
                            proxy.invoke('getBooks',{Localisation: 'All'});
                            this.setState({ 
                            signalrConnected: true,
                            canProcessBarcode: true
              });
            })
            
        }).fail((e) => {
            ToastAndroid.show('signalr start connection error: ' + e, ToastAndroid.SHORT);
        });

        connection.disconnected(function() {
          setTimeout(function(){
              connection.start().done(() => { 
                ToastAndroid.show('signalr connected', ToastAndroid.SHORT);
                proxy.invoke('login',{FacebookId: DeviceInfo.getUniqueID(), UserName: 'Maurice' })
                      .done(() => { proxy.invoke('getBooks',{Localisation: 'All'}); })
              }).fail((e) => {
                  ToastAndroid.show('reconnecting failed: ' + e, ToastAndroid.SHORT);
                });
          }.bind(this), 5000)
        })

        connection.error(function (error) {
            ToastAndroid.show('signalr connection error ' + error, ToastAndroid.SHORT);
            this.setState({
              signalrConnected: false
            })
        }.bind(this));
        
        BackAndroid.addEventListener('hardwareBackPress', this._onBackButtonPressed.bind(this));
  }

  _onBackButtonPressed() {
    if (this.state.currentView == 'Books') {
            return true;
          }
        return false;
  }

  _barcodeReceived(e) {
    if (!this.state.canProcessBarcode) {
      return;
    }
    this.setState({ canProcessBarcode: false })
    ToastAndroid.show('Barcode: ' + e.data, ToastAndroid.SHORT);
    proxy.invoke('findBooks',{ISBN: e.data})
    .fail((error) => {
      this.setState({ 
        currentView: 'Books'
      });
      ToastAndroid.show('error: ' + error, ToastAndroid.LONG)
    });
  }

  _updateBook(book) {
    proxy.invoke('updateBook', {Book: book});
  }

  _registerBook(book) {
      proxy.invoke('registerBook', book);

      this.setState({ currentView: 'Books' })
  }

    _onRefresh() {
        this.setState({refreshing: true});
        try {
          connection.start().done(() => { 
            ToastAndroid.show('signalr connected', ToastAndroid.SHORT);
            proxy.invoke('login',{FacebookId: DeviceInfo.getUniqueID(), UserName: 'Maurice' })
                      .done(() => { proxy.invoke('getBooks',{Localisation: 'All'}); })
          });
        } catch(e) {
          ToastAndroid.show(e, ToastAndroid.SHORT)
        }
        
        this.setState({refreshing: false});
    }

  _openDrawer() {
    this.refs['DRAWER_REF'].openDrawer();
  }

  _navigateToBooksList() {
    this.setState({ currentView: 'Books' });
  }

  _navigateToBookCopies(books) {
    if (books === undefined) {
      this.setState({ currentView: 'Copies' });
    } else {
      this.setState({ currentView: 'Copies', selectedCopies: books });
    }
  }

  _navigateToBook(book) {
    this.setState({ 
      currentView: 'Book', 
      currentBook: book,
      navigateFromCopies: false
    });
  }

  _navigateToRegisterBook(book) {
    this.setState({ 
      currentView: 'RegisterBook', 
      newBook: book,
      navigateFromFoundBooks: true
    });
  }

  _navigateToBookFromCopies(book) {
    this.setState({ 
      currentView: 'Book', 
      currentBook: book,
      navigateFromCopies: true
    });
  }

  _navigateToSelectBookToRegister() {
    this.setState({ 
      currentView: 'SelectBookToRegister',
    });
  }
  
  _closeDrawer() {
    this.refs['DRAWER_REF'].closeDrawer();
  }

  _navigateToScanView() {
    this.setState({ 
      currentView: 'Scan' 
    });
  }

  _navigateToAddBookView() {
    this.setState({
      currentView: 'InsertIsbn',
    });
  }

  _renderActiveView() {
      switch(this.state.currentView)
      {
        case 'Books':
          return(
            <BookList 
              books={ this.state.books }
              onAddBookManually={ this._navigateToAddBookView.bind(this) }
              onAddBookUsingCamera= { this._navigateToScanView.bind(this) }
              onLeftElementPress={() => this._openDrawer.bind(this) } 
              onSingleBookPress={ this._navigateToBook.bind(this) }
              onBookGroupPress={ this._navigateToBookCopies.bind(this) }
              onRefresh={ () => this._onRefresh.bind(this) }
              refreshing={ this.state.refreshing } />
          );
        case 'Scan':
          return(
            <ScanView onBarCodeRead={ () => this._barcodeReceived.bind(this) } onBackButtonPress={ () => this._navigateToBooksList.bind(this)} />
          );
        case 'Book':
          if (this.state.navigateFromCopies) {
            return(
              <Book book={ this.state.currentBook } onSaveButtonPress={ this._updateBook.bind(this)} onBackButtonPress={ () => this._navigateToBookCopies.bind(this)}/>              
            );  
          }
          return(
            <Book book={ this.state.currentBook } onSaveButtonPress={ this._updateBook.bind(this)} onBackButtonPress={ () => this._navigateToBooksList.bind(this)}/>              
          );
        case 'Copies':
          return(
            <BookCopies books={ this.state.selectedCopies } onLeftElementPress={ () => this._navigateToBooksList.bind(this) } onElementPress={ this._navigateToBookFromCopies.bind(this) } />
          );
        case 'RegisterBook':
          if (this.state.navigateFromFoundBooks) {
            return(
              <NewBook book={ this.state.newBook } onSaveButtonPress={ this._registerBook.bind(this) } onBackButtonPress={ () => this._navigateToSelectBookToRegister.bind(this)} />
            );  
          }
          return(
            <NewBook book={ this.state.newBook } onSaveButtonPress={ this._registerBook.bind(this) } onBackButtonPress={ () => this._navigateToBooksList.bind(this)} />
          );
        case 'SelectBookToRegister':
          return(
            <SelectBookToRegister books={this.state.newBooks} onElementPress={ this._navigateToRegisterBook.bind(this) } onLeftElementPress={ () => this._navigateToBooksList.bind(this)}/>
          );
        case 'InsertIsbn':
          return(
            <InsertIsbn onSaveButtonPress={ this._barcodeReceived.bind(this) } onBackButtonPress={ () => this._navigateToBooksList.bind(this)}/>
          );
      }
    }

  render() {
    var navigationView = (
      <Drawer>
            <Drawer.Header >
                <Drawer.Header.Account
                    avatar={<Avatar text={'M'} />}
                    accounts={[
                        { avatar: <Avatar text="B" /> }
                    ]}
                    footer={{
                        dense: true,
                        centerElement: {
                            primaryText: 'Maurice',
                            secondaryText: 'slawomir.rudawski@gmail.com',
                        },
                        rightElement: 'arrow-drop-down',
                    }}
                />
            </Drawer.Header>
            <Drawer.Section
                divider
                items={[
                    { icon: 'local-library', value: 'Books', onPress: this._navigateToBooksList.bind(this) },
                    { icon: 'fullscreen', value: 'Scan Book', onPress: this._navigateToScanView.bind(this) },
                ]}
            />
        </Drawer>
    );

    return (
      <ThemeProvider uiTheme={uiTheme}>
        <DrawerLayoutAndroid
          ref={'DRAWER_REF'}
          drawerWidth={300}
          drawerPosition={DrawerLayoutAndroid.positions.Left}
          renderNavigationView={() => navigationView}>
          { this._renderActiveView() }
        </DrawerLayoutAndroid>
      </ThemeProvider>
    );
  }
}

const uiTheme = {
    palette: {
        primaryColor: COLOR.green500,
    },
    toolbar: {
        container: {
            height: 50,
        },
    },
    button: {
      container:{

      }
    }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  scanIsbnButton: {
    padding:10, 
    height:45, 
    overflow:'hidden', 
    borderRadius:4, 
    backgroundColor: '#337ab7'
  },
  scanIsbnButtonText: {
    fontSize: 20, 
    color: 'white',
    textAlign: 'center'
  },
  registerBookButton: {
    padding:10, 
    height:45, 
    overflow:'hidden', 
    borderRadius:4, 
    backgroundColor: '#5cb85c'
  },
  registerBookButtonText: {
    fontSize: 20, 
    color: 'white',
    textAlign: 'center'
  },
  input: {
      flex: 1,
      flexDirection: 'row',
      width: 200
  }
});

AppRegistry.registerComponent('HomeLibrary', () => HomeLibrary);
