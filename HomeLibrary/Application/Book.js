import React, { Component, PropTypes } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  DrawerLayoutAndroid,
  ScrollView,
  TextInput,
  BackAndroid,
  Image,
  Picker,
  ToastAndroid
} from 'react-native';
import { COLOR, ThemeProvider, ActionButton, Drawer, Toolbar,Button, ListItem, Avatar } from 'react-native-material-ui';

const propTypes = {
    book: PropTypes.object,
    onBackButtonPress: PropTypes.func.isRequired
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

export default class Book extends Component {
    constructor(props) {
        super(props);
        var book = this.props.book;
        this.state = {
            author: book.Author,
            title: book.Title,
            ISBN: book.ISBN,
            description: book.Description,
            publishDate: book.PublishDate,
            publisher: book.Publisher,
            localisation: book.Localisation,
            condition: book.Condition,
            cover: book.Cover,
            edit: false
         };
         BackAndroid.addEventListener('hardwareBackPress', this.props.onBackButtonPress());
    }

    _onEditButtonPress() {
        this.setState({ edit: true })
    }

    _onActionButtonPress(text) {
        if (text === 'done') {
            this.props.onSaveButtonPress({
                                            BookId: this.props.book.BookId,
                                            Title: this.state.title, 
                                            Author: this.state.author, 
                                            ISBN: this.state.ISBN,
                                            Localisation: this.state.localisation,
                                            Description: this.state.description,
                                            Publisher: this.state.publisher,
                                            PublishDate: this.state.publishDate,
                                            Cover: this.state.cover,
                                            Condition: this.state.condition
                                        });
            this.setState({ edit: false }) 
        }    
                                                      
    }

    _renderActionButton() {
        if (this.state.edit) {
            return <ActionButton icon='save' transition='speedDial' actions={['done']} onPress={ this._onActionButtonPress.bind(this)} />
        }
        return <ActionButton icon='edit' onPress={ this._onEditButtonPress.bind(this) } />
    }

    render() {
        var actionButton = this._renderActionButton();
        var image;
        if (this.props.book.Cover === '') {
            image = <View />
        } else {
            image = <Image style={{width: 200, height: 283}} source={ {uri: this.props.book.Cover }}/>
        }
        return (
            <View>
                <Toolbar
                    leftElement="arrow-back"
                    onLeftElementPress={ this.props.onBackButtonPress() }
                    centerElement="Book"
                />
                <ScrollView style={{height: 520 }}>
                    <View style={{ marginLeft: 10, marginTop: 10, justifyContent: 'center'}}>
                        { image }
                        <Text>Lokalizacja: </Text>
                        <Picker enabled={this.state.edit} selectedValue={this.state.localisation}  onValueChange={(loc) => this.setState({localisation: loc})}>
                            <Picker.Item label="Imielin" value="Imielin" />
                            <Picker.Item label="Gliwice" value="Gliwice" />
                            <Picker.Item label="Niewiadomo" value="None" />
                        </Picker>
                        <Text>Stan: </Text>
                        <Picker enabled={this.state.edit} selectedValue={this.state.condition} onValueChange={(cond) => this.setState({condition: cond})}>
                            <Picker.Item label="Nowa" value="New" />
                            <Picker.Item label="OK" value="Good" />
                            <Picker.Item label="Zły" value="Bad" />
                            <Picker.Item label="Rozlatuje się" value="VeryBad" />
                            <Picker.Item label="Niewiadomo" value="None" />
                        </Picker>
                        <Text>Autor: </Text>
                        <TextInput 
                            editable={this.state.edit}
                            value={this.state.author} 
                            onChangeText={(text) => {this.setState({author: text})}}/> 
                        <Text>Tytuł: </Text>
                        <TextInput 
                            editable={this.state.edit}
                            value= {this.state.title}
                            onChangeText={(text) => {this.setState({title: text})}}/>
                        <Text>ISBN:</Text>
                        <TextInput 
                            editable={this.state.edit}
                            value= {this.state.ISBN}
                            onChangeText={(text) => {this.setState({ISBN: text})}}/>
                        <Text>Opis: </Text>
                        <TextInput 
                            editable={this.state.edit}
                            multiline= {true}
                            value= {this.state.description}
                            onChangeText={(text) => {this.setState({description: text})}}/>
                        <Text>Wydawnictwo:</Text>
                        <TextInput 
                            editable={this.state.edit}
                            value= {this.state.publisher}
                            onChangeText={(text) => {this.setState({publisher: text})}}/>
                        <Text>Data wydania:</Text>
                        <TextInput 
                            editable={this.state.edit}
                            value= {this.state.publishDate}
                            onChangeText={(text) => {this.setState({publishDate: text})}}/>
                    </View>
                </ScrollView>
                { actionButton }
            </View>
        );
    }
}

Book.propTypes = propTypes;