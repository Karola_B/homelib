import React, { Component, PropTypes } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  DrawerLayoutAndroid,
  ScrollView,
  BackAndroid,
  RefreshControl
} from 'react-native';

import { COLOR, ThemeProvider, ActionButton, Drawer, Toolbar,Button, ListItem, Avatar } from 'react-native-material-ui';
import BookListItem from './BookListItem'
import LocalisationAvatar from './LocalisationAvatar'

const propTypes = {
    books: PropTypes.array.isRequired,
    onElementPress: PropTypes.func.isRequired,
    onLeftElementPress: PropTypes.func.isRequired,
};
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

export default class BookCopies extends Component {

    constructor(props) {
        super(props);
    }

    _renderCopies() {
        var bookList = this.props.books
                .map(function(book, i) {
                    return( 
                        <ListItem key={i} numberOfLines={2} leftElement={ <LocalisationAvatar localisation={ book.Localisation } />}
                        centerElement={ book.Title } 
                        onPress={ () => this.props.onElementPress(book) }/>
                    );     
                }.bind(this));

        return(
            <ScrollView
                refreshControl = {
                    <RefreshControl
                        refreshing={false}
                         />
                }
                keyboardShouldPersistTaps 
                keyboardDismissMode="interactive">
                { bookList }
            </ScrollView>
        );
    }

    render() {
        return (
            <View style={{flex: 1}}>
                <Toolbar
                    leftElement="arrow-back"
                    onLeftElementPress={ this.props.onLeftElementPress() }
                    centerElement="Copies"
                />
                { this._renderCopies() }
            </View>
        );
    }
}

BookCopies.propTypes = propTypes;