import React, { Component, PropTypes } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  DrawerLayoutAndroid,
  ScrollView,
  BackAndroid,
  RefreshControl
} from 'react-native';

import { COLOR, ThemeProvider, ActionButton, Drawer, Toolbar,Button, ListItem, Avatar } from 'react-native-material-ui';
import LocalisationAvatar from './LocalisationAvatar'

const propTypes = {
    books: PropTypes.array.isRequired,
    onElementPress: PropTypes.func.isRequired,
    onLeftElementPress: PropTypes.func.isRequired,
};
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

export default class SelectBookToRegister extends Component {

    constructor(props) {
        super(props);
    }

    _getLeftElementIcon(provider) {
        switch (provider) {
            case 'ksiazki.org':
                return "book";
            case 'Library':
                return "local-library";
            case 'Google':
                return "cloud";
            default:
                return ;
        }
    }

    _renderFoundBooks() {
        var bookList = this.props.books
                .map(function(book, i) {
                    var leftElementIcon = this._getLeftElementIcon(book.Provider);
                    return( 
                        <ListItem key={i} numberOfLines={2} leftElement={ leftElementIcon }
                        centerElement={ book.Title } 
                        onPress={ () => this.props.onElementPress(book) }/>
                    );     
                }.bind(this));

        return(
            <ScrollView
                refreshControl = {
                    <RefreshControl
                        refreshing={false}
                         />
                }
                keyboardShouldPersistTaps 
                keyboardDismissMode="interactive">
                { bookList }
            </ScrollView>
        );
    }

    render() {
        return (
            <View style={{flex: 1}}>
                <Toolbar
                    leftElement="arrow-back"
                    onLeftElementPress={ this.props.onLeftElementPress() }
                    centerElement="Found books"
                />
                { this._renderFoundBooks() }
            </View>
        );
    }
}

SelectBookToRegister.propTypes = propTypes;