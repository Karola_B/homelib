import React, { Component, PropTypes } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  DrawerLayoutAndroid,
  ScrollView,
  BackAndroid,
  RefreshControl
} from 'react-native';

import { COLOR, ThemeProvider, ActionButton, Drawer, Toolbar,Button, ListItem, Avatar } from 'react-native-material-ui';
import BookListItem from './BookListItem'
import LocalisationAvatar from './LocalisationAvatar'

const propTypes = {
    books: PropTypes.array.isRequired,
    onAddBookUsingCamera: PropTypes.func.isRequired,
    onAddBookManually: PropTypes.func.isRequired,
    onSingleBookPress: PropTypes.func.isRequired,
    onBookGroupPress: PropTypes.func.isRequired,
    onLeftElementPress: PropTypes.func.isRequired,
    onRefresh: PropTypes.func.isRequired,
    refreshing: PropTypes.bool.isRequired
};
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

export default class BookList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            searchText: '',
            refreshing: false,
            chooseAddMode: false
        }
        
    }

    _renderBooksList() {
        var bookList = this.props.books
                .filter(function(books,i) { 
                    return this.state.searchText.length == 0 || books[0].Title.toLowerCase().indexOf(this.state.searchText.toLowerCase()) > -1 
                }.bind(this))
                .map(function(books, i) {
                    if (books.length === 1) {
                        return( 
                            <ListItem key={i} numberOfLines={2} leftElement={ <LocalisationAvatar localisation={ books[0].Localisation } />}
                                centerElement={ books[0].Title } 
                                onPress={ () => this.props.onSingleBookPress(books[0]) }/>
                        );     
                    }
                    return (
                        <ListItem key={i} numberOfLines={2} leftElement={  <LocalisationAvatar localisation={ books.length.toString() } /> }
                            centerElement={ books[0].Title } 
                            onPress={ () => this.props.onBookGroupPress(books) }/>
                    );
                    
                }.bind(this));

        return(
            <ScrollView
                refreshControl = {
                    <RefreshControl
                        refreshing={this.props.refreshing}
                        onRefresh={ this.props.onRefresh()}
                         />
                }
                keyboardShouldPersistTaps 
                keyboardDismissMode="interactive">
                { bookList }
            </ScrollView>
        );
    }

    _onAddBookButtonPress() {
        this.setState({ chooseAddMode: true })
    }

    _onActionButtonPress(command) {
        if (command == 'camera') {
            this.props.onAddBookUsingCamera();
        } else if (command == 'edit') {
            this.props.onAddBookManually();
        }
    }

    _renderActionButton() {
        if (this.state.chooseAddMode) {
            return (
                <ActionButton transition='speedDial' actions={['camera','edit']} onPress={ this._onActionButtonPress.bind(this)} />
            );
        }
        else {
            return (
                <ActionButton onPress={ this._onAddBookButtonPress.bind(this) }/>   
            );
        }
    }

    render() {
        var actionButton = this._renderActionButton();

        return (
            <View style={{flex: 1}}>
                <Toolbar
                    leftElement="menu"
                    onLeftElementPress={ this.props.onLeftElementPress() }
                    centerElement="Books"
                    searchable={{ 
                        autofocus: true,
                        placeholder: 'Search',
                        onChangeText: value => this.setState({ searchText: value }),
                        onSearchClosed: () => this.setState({ searchText: ''})
                    }}
                />
                { this._renderBooksList() }
                { actionButton } 
            </View>
        );
    }
}

BookList.propTypes = propTypes;