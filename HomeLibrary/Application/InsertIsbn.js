import React, { Component, PropTypes } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  DrawerLayoutAndroid,
  ScrollView,
  TextInput,
  BackAndroid,
  Image,
  Picker,
  ToastAndroid
} from 'react-native';
import { COLOR, ThemeProvider, ActionButton, Drawer, Toolbar,Button, ListItem, Avatar } from 'react-native-material-ui';

const propTypes = {
    onSaveButtonPress: PropTypes.func.isRequired,
    onBackButtonPress: PropTypes.func.isRequired
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

export default class InsertIsbn extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ISBN: '',
         };
         BackAndroid.addEventListener('hardwareBackPress', this.props.onBackButtonPress());
    }

    _onActionButtonPress(text) {
        if (text === 'done') {
            this.props.onSaveButtonPress({
                                            BookId: this.props.book.BookId,
                                            Title: this.state.title, 
                                            Author: this.state.author, 
                                            ISBN: this.state.ISBN,
                                            Localisation: this.state.localisation,
                                            Description: this.state.description,
                                            Publisher: this.state.publisher,
                                            PublishDate: this.state.publishDate,
                                            Cover: this.state.cover,
                                            Condition: this.state.condition
                                        });
            this.setState({ edit: false }) 
        }    
                                                      
    }

    _onSaveButtonPress() {
        this.props.onSaveButtonPress({data:this.state.ISBN});
    }

    _renderActionButton() {
        return <ActionButton icon='save' onPress={ this._onSaveButtonPress.bind(this) } />
    }

    render() {
        var actionButton = this._renderActionButton();

        return (
            <View>
                <Toolbar
                    leftElement="arrow-back"
                    onLeftElementPress={ this.props.onBackButtonPress() }
                    centerElement="Book"
                />
                <ScrollView style={{height: 520 }}>
                    <View style={{ marginLeft: 10, marginTop: 10, justifyContent: 'center'}}>
                        <Text>ISBN:</Text>
                        <TextInput 
                            editable={this.state.edit}
                            value= {this.state.ISBN}
                            onChangeText={(text) => {this.setState({ISBN: text})}}/>
                    </View>
                </ScrollView>
                { actionButton }
            </View>
        );
    }
}

InsertIsbn.propTypes = propTypes;