import React, { Component, PropTypes } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  DrawerLayoutAndroid,
  ScrollView,
  TextInput,
  BackAndroid,
  Image,
  Picker
} from 'react-native';
import { COLOR, ThemeProvider, ActionButton, Drawer, Toolbar,Button, ListItem, Avatar } from 'react-native-material-ui';
import Book from './Book'
const propTypes = {
    book: PropTypes.object.isRequired,
    onBackButtonPress: PropTypes.func.isRequired,
    onSaveButtonPress: PropTypes.func.isRequired
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

export default class NewBook extends Component {
    constructor(props) {
        super(props);
        var book = this.props.book;

        if (book === null) {
            this.state = {
                author: '',
                cover: '',
                title: '',
                ISBN: '',
                description: '',
                publishDate: '',
                publisher: '',
                localisation: 'None',
                condition: 'None'
            };
        } else {
            this.state = {
                author: book.Author,
                cover: book.Cover,
                title: book.Title,
                ISBN: book.ISBN,
                description: book.Description,
                publishDate: book.PublishDate,
                publisher: book.Publisher,
                localisation: book.Localisation,
                condition: book.Condition
            };
        }
         BackAndroid.addEventListener('hardwareBackPress', this.props.onBackButtonPress());
    }
    render() {
        var image;
        if (this.state.cover === '') {
            image = <View />
        } else {
            image = <Image style={{width: 200, height: 283}} source={ {uri: this.state.cover }}/>
        }
        return (
            <View style={{flex: 1}}>
                <View>
                    <Toolbar
                        leftElement="arrow-back"
                        onLeftElementPress={ this.props.onBackButtonPress() }
                        centerElement="Book"
                    />
                    <ScrollView style={{height: 520 }}>
                        <View style={{ marginLeft: 10, marginTop: 10, justifyContent: 'center'}}>
                            { image }
                            <Text>Lokalizacja: </Text>
                            <Picker selectedValue={this.state.localisation} onValueChange={(loc) => this.setState({localisation: loc})}>
                                <Picker.Item label="Imielin" value="Imielin" />
                                <Picker.Item label="Gliwice" value="Gliwice" />
                                <Picker.Item label="Niewiadomo" value="None" />
                            </Picker>
                            <Text>Stan: </Text>
                            <Picker selectedValue={this.state.condition} onValueChange={(cond) => this.setState({condition: cond})}>
                                <Picker.Item label="Nowa" value="New" />
                                <Picker.Item label="OK" value="Good" />
                                <Picker.Item label="Zły" value="Bad" />
                                <Picker.Item label="Rozlatuje się" value="VeryBad" />
                                <Picker.Item label="Niewiadomo" value="None" />
                            </Picker>
                            <Text>Autor: </Text>
                            <TextInput 
                                value={this.state.author} 
                                onChangeText={(text) => {this.setState({author: text})}}/> 
                            <Text>Tytuł: </Text>
                            <TextInput 
                                value= {this.state.title}
                                onChangeText={(text) => {this.setState({title: text})}}/>
                            <Text>ISBN:</Text>
                            <TextInput 
                                value= {this.state.ISBN}
                                onChangeText={(text) => {this.setState({ISBN: text})}}/>
                            <Text>Opis: </Text>
                            <TextInput 
                                value= {this.state.description}
                                onChangeText={(text) => {this.setState({description: text})}}/>
                            <Text>Wydawnictwo:</Text>
                            <TextInput 
                                value= {this.state.publisher}
                                onChangeText={(text) => {this.setState({publisher: text})}}/>
                            <Text>Data wydania:</Text>
                            <TextInput 
                                value= {this.state.publishDate}
                                onChangeText={(text) => {this.setState({publishDate: text})}}/>
                        </View>
                    </ScrollView>
                </View>
                <ActionButton icon='done' onPress={ () => this.props.onSaveButtonPress({ Book: {
                                                                                                Title: this.state.title, 
                                                                                                Author: this.state.author, 
                                                                                                ISBN: this.state.ISBN,
                                                                                                Localisation: this.state.localisation,
                                                                                                Description: this.state.description,
                                                                                                Publisher: this.state.publisher,
                                                                                                PublishDate: this.state.PublishDate,
                                                                                                Cover: this.state.cover,
                                                                                                Condition: this.state.condition
                                                                                            }}) } />
            </View>
        );
    }
}

NewBook.propTypes = propTypes;