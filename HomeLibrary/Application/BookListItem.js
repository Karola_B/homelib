import React, { Component, PropTypes } from 'react';
import { View, StyleSheet, Text } from 'react-native';

import { COLOR, ThemeProvider, ActionButton, Drawer, Toolbar,Button, ListItem, Avatar } from 'react-native-material-ui';
import LocalisationAvatar from './LocalisationAvatar'

const propTypes = {
    asdf: PropTypes.number.isRequired,
    book: PropTypes.object.isRequired,
    onPress: PropTypes.func
};
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

export default class BookListItem extends Component {
    render() {
        return (
            <ListItem key={this.props.book.BookId}  
                numberOfLines={2} 
                leftElement={ <LocalisationAvatar localisation={ this.props.book.Localisation } />}
                centerElement={ this.props.book.Title } 
                onPress={ this.props.onPress(this.props.book).bind(this) }/>
        );
    }
}

BookListItem.propTypes = propTypes;