import { View, StyleSheet, Text, BackAndroid } from 'react-native';
import React, { Component, PropTypes } from 'react';
import BarcodeScanner from 'react-native-barcodescanner';

const propTypes = {
    onBarCodeRead: PropTypes.func.isRequired,
    onBackButtonPress: PropTypes.func.isRequired
};
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

export default class ScanView extends Component {
    constructor(props) {
        super(props);
        BackAndroid.addEventListener('hardwareBackPress', this.props.onBackButtonPress());
    }
    render() {
        return (
            <BarcodeScanner
              onBarCodeRead={ this.props.onBarCodeRead() }
              style={{ flex: 1 }}
              torchMode={'on'}
              cameraType={'back'}
            />
        );
    }
}

ScanView.propTypes = propTypes;