import { View, StyleSheet, Text } from 'react-native';
import React, { Component, PropTypes } from 'react';

const propTypes = {
    localisation: PropTypes.string.isRequired,
};
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

class LocalisationAvatar extends Component {
    render() {
        if (this.props.localisation[0] === 'I') {
            return(
                    <View style={{backgroundColor: '#337ab7', borderRadius: 50, width: 40, height: 40, alignItems: 'center',justifyContent: 'center'}}>
                        <Text style={{ opacity: 1, color: 'white' }}>
                            {this.props.localisation[0]}
                        </Text>
                    </View>

                );
        } else if (this.props.localisation[0] === 'G') {
            return(
                    <View style={{backgroundColor: '#4fc1e9', borderRadius: 50, width: 40, height: 40, alignItems: 'center',justifyContent: 'center'}}>
                        <Text style={{ opacity: 1, color: 'white' }}>
                            {this.props.localisation[0]}
                        </Text>
                    </View>

                );
        } else if (this.props.localisation[0] === 'N') {
            return(
                    <View style={{backgroundColor: '#f0ad4e', borderRadius: 50, width: 40, height: 40, alignItems: 'center',justifyContent: 'center'}}>
                        <Text style={{ opacity: 1, color: 'white' }}>
                            {this.props.localisation[0]}
                        </Text>
                    </View>

                );
        } else {
            return(
            <View style={{backgroundColor: '#337ab7', borderRadius: 50, width: 40, height: 40, alignItems: 'center',justifyContent: 'center'}}>
                <Text style={{ opacity: 1, color: 'white' }}>
                    {this.props.localisation[0]}
                </Text>
            </View>
            );
        }
    }
}

LocalisationAvatar.propTypes = propTypes;

export default LocalisationAvatar;