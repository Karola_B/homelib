namespace HomeLib.Services
{
    public interface IAuthenticationService
    {
        object ValidateConnection(string connectionId);
    }
}