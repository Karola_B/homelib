﻿namespace HomeLib.Services
{
    public class BookDto
    {
        public string Author { get; set; }
        public string Title { get; set; }
        public string Localisation { get; set; }
        public string ISBN { get; set; }
        public string Description { get; set; }
        public string Publisher { get; set; }
        public string PublishDate { get; set; }
        public string Cover { get; set; }
        public string Condition { get; set; }
        public int BookId { get; set; }
        public string Provider { get; set; }
    }
}