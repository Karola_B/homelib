using HomeLibrary.Data.Models;
using HomeLibrary.Data.Repositories;

namespace HomeLib.Services
{
    public class UsersService : IUsersService
    {
        private readonly IUsersRepository _usersRepository;

        public UsersService(IUsersRepository usersRepository)
        {
            _usersRepository = usersRepository;
        }

        public User RegisterUser(string facebookId, string userName, string connectionId)
        {
            var existingUser = _usersRepository.GetUserByFacebookId(facebookId);

            if (existingUser == null)
            {
                _usersRepository.AddUser(new User {FacebookId = facebookId, Login = userName, ConnectionId = connectionId});
            }
            else
            {
                existingUser.ConnectionId = connectionId;
                _usersRepository.SaveUser(existingUser);
            }

            return _usersRepository.GetUserByFacebookId(facebookId);
        }

        public void LogoutUserWithFacebookId(string facebookId)
        {
            var userByFacebookId = _usersRepository.GetUserByFacebookId(facebookId);
            userByFacebookId.ConnectionId = string.Empty;
            _usersRepository.SaveUser(userByFacebookId);
        }

        public User GetByConnection(string connectionId)
        {
            return _usersRepository.GetUserByConnection(connectionId);
        }

        public void LogoutUserWithConnectionId(string connectionId)
        {
            var user = _usersRepository.GetUserByConnection(connectionId);
            user.ConnectionId = string.Empty;
            _usersRepository.SaveUser(user);
        }
    }
}