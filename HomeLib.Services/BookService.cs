﻿using System;
using System.Collections.Generic;
using System.Linq;
using HomeLib.BooksInformationService;
using HomeLibrary.Data.Models;
using HomeLibrary.Data.Repositories;

namespace HomeLib.Services
{
    public class BookService : IBookService
    {
        private readonly IEnumerable<IBooksInformationService> _bookInfoService;
        private readonly IBooksRepository _bookRepository;

        public BookService(IEnumerable<IBooksInformationService> bookInfoService, IBooksRepository bookRepository)
        {
            _bookInfoService = bookInfoService;
            _bookRepository = bookRepository;
        }

        public IList<Book> GetBooksForLocalisation(LocalisationEnum localisation)
        {
            return _bookRepository.GetBooksForLocalisation(localisation);
        }

        public int RegisterBook(BookDto bookCandidate)
        {
            ConditionEnum condition;
            if (!Enum.TryParse(bookCandidate.Condition, out condition))
            {
                condition = ConditionEnum.None;
            }

            LocalisationEnum localisation;
            if (!Enum.TryParse(bookCandidate.Localisation, out localisation))
            {
                localisation = LocalisationEnum.None;
            }

            var book = new Book
            {
                Title = bookCandidate.Title,
                Author = bookCandidate.Author,
                ISBN = bookCandidate.ISBN,
                Localisation = localisation,
                Description = bookCandidate.Description,
                Publisher = bookCandidate.Publisher,
                PublishDate = bookCandidate.PublishDate,
                Cover = bookCandidate.Cover,
                Condition = condition
            };

            return _bookRepository.AddBookToLibrary(book);
        }

        public IList<BookDto> FindBooks(string isbn)
        {
            var foundBooks = new List<BookDto>();

            var existingBooks = _bookRepository.GetBooksByIsbn(isbn);

            if (existingBooks.Any())
            {
                foundBooks.AddRange(MapToBookDto(existingBooks));
                foundBooks.ForEach(x => x.Provider = "Library");
            }

            foreach (var booksInformationService in _bookInfoService)
            {
                var bookInformation = booksInformationService.GetByIsbn(isbn);
                if (bookInformation != null)
                {
                    foundBooks.Add(
                        new BookDto
                        {
                            Title = bookInformation.Title,
                            Author = bookInformation.Author,
                            ISBN = isbn,
                            Localisation = LocalisationEnum.None.ToString(),
                            Description = bookInformation.Description,
                            PublishDate = bookInformation.PublishedDate,
                            Publisher = bookInformation.Publisher,
                            Cover = bookInformation.Cover,
                            Provider = bookInformation.Provider
                        }
                    );
                }
            }
            return foundBooks;
        }

        public void UpdateBook(BookDto book)
        {
            var existingBook = _bookRepository.GetBookById(book.BookId);

            if (existingBook == null)
            {
                return;
            }

            ConditionEnum condition;
            if (!Enum.TryParse(book.Condition, out condition))
            {
                condition = ConditionEnum.None;
            }

            LocalisationEnum localisation;
            if (!Enum.TryParse(book.Localisation, out localisation))
            {
                localisation = LocalisationEnum.None;
            }

            existingBook.Title = book.Title;
            existingBook.Author = book.Author;
            existingBook.ISBN = book.ISBN;
            existingBook.Localisation = localisation;
            existingBook.Condition = condition;
            existingBook.Description = book.Description;
            existingBook.PublishDate = book.PublishDate;
            existingBook.Publisher = book.Publisher;
            existingBook.Cover = book.Cover;

            _bookRepository.UpdateBook(existingBook);
        }

        private static BookDto[] MapToBookDto(IList<Book> books)
        {
            return books.Select(x =>
                new BookDto
                {
                    BookId = x.BookId,
                    Author = x.Author,
                    Title = x.Title,
                    ISBN = x.ISBN,
                    Description = x.Description,
                    Publisher = x.Publisher,
                    PublishDate = x.PublishDate,
                    Cover = x.Cover,
                    Localisation = x.Localisation.ToString(),
                    Condition = x.Condition.ToString()
                }).ToArray();
        }

        public int RegisterBookCopy(BookDto bookCopy)
        {
            throw new NotImplementedException();
        }
    }
}