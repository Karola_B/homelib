using HomeLibrary.Data.Repositories;

namespace HomeLib.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly IUsersRepository _usersRepository;

        public AuthenticationService(IUsersRepository usersRepository)
        {
            _usersRepository = usersRepository;
        }

        public object ValidateConnection(string connectionId)
        {
            var userByConnection = _usersRepository.GetUserByConnection(connectionId);

            if (userByConnection != null)
            {
                return new object();
            }

            return null;
        }
    }
}