using HomeLibrary.Data.Models;

namespace HomeLib.Services
{
    public interface IUsersService
    {
        User RegisterUser(string facebookId, string userName, string connectionId);
        void LogoutUserWithFacebookId(string facebookId);
        User GetByConnection(string connectionId);
        void LogoutUserWithConnectionId(string connectionId);
    }
}