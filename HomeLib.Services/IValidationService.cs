﻿namespace HomeLib.Services
{
    public interface IValidationService
    {
        bool ValidateIsbn(string isbn);
    }
}