﻿using System.Collections.Generic;
using HomeLibrary.Data.Models;

namespace HomeLib.Services
{
    public interface IBookService
    {
        IList<Book> GetBooksForLocalisation(LocalisationEnum localisation);
        int RegisterBook(BookDto bookCandidate);
        IList<BookDto> FindBooks(string isbn);
        void UpdateBook(BookDto book);
        int RegisterBookCopy(BookDto bookCopy);
    }
}