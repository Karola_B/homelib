﻿namespace Switter
{
    internal interface ISwitterHubEventBus
    {
        void CallBegins(string name, bool isActive);
    }
}