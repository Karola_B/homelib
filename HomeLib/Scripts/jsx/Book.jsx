﻿class Book extends React.Component {
    constructor(props) {
        super(props);
    }
    
    _formatDate () {
        return moment(new Date(this.props.book.DateTime)).format("YYYY-MM-DD HH:mm:ss");
    }

    render () {
        return (
            <div className='bg-primary book-body'>
                <h3>
                    { this.props.book.Title }
                </h3>
                <div className='book-entry-author'>
                    { this.props.book.Author }
                </div>
                <div className='book-date'>
                    { this.props.book.Localisation }
                </div>
                <div className='book-quantity'>
                    { this.props.book.Quantity}
                </div>
            </div>
        );
    }
};