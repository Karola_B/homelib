﻿class Member extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        var avatarLink = "https://api.skype.com/users/" + this.props.member.SkypeLogin + "/profile/avatar";
        return (
            <div className="col-sm-1 member-name">
                <img className="avatar" src={ avatarLink } title={ this.props.member.Name }/>
            </div>
        );
    }
};