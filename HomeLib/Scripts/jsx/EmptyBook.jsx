﻿class EmptyBook extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className='empty-book-body'>
                <h3>
                    Nothing to show... Add something!
                </h3>
                <div className='book-entry-author'>
                    @Library
                </div>
            </div>
        );
    }
}