﻿class NewTweet extends React.Component {
    constructor(props) {
        super(props);
    }
    
    _formatDate () {
        return moment(new Date(this.props.tweet.DateTime)).format("YYYY-DD-MM HH:mm:ss");
    }

    render () {
        return (
                <div className='new-tweet-view'>
                    <h1>
                        {this.props.tweet.Message}
                    </h1>
                    <div className='tweet-author'>
                        <h3>@{ this.props.tweet.Author }</h3>
                    </div>
                    <div className='tweet-date'>
                        <h3>{ this._formatDate() }</h3>
                    </div>
                </div>
        );
    }
};