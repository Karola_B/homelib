﻿class Dashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            bookGroups: [],
            clientRegistered: false,
            detailsFormValid: true,
            borrowers: [],
            userName: ""
        };
        var sHub;
    }
  
    componentDidMount() {
        $.connection.hub.url = "/signalr";

        sHub = $.connection.library;

        sHub.client.booksChanged = function (books) {
            this.setState({ bookGroups: books });
        }.bind(this);

        sHub.client.userRegistered = function (response) {
            this.setState({ clientRegistered: true });
            this._getLibraryState();
        }.bind(this);

        sHub.client.userNotRegistered = function (response) {
            this.setState({ detailsFormValid: false });
        }.bind(this);

        sHub.client.libraryStateReceived = function (response) {
            this.setState({ bookGroups: response.Books, borrowers: response.Borrowers, clientRegistered: response.Registered });
        }.bind(this);

        sHub.client.bookRegistered = function (book) {
            this._getLibraryState();
        }.bind(this);

        sHub.client.borrowerCreated = function (borrower) {
            var borrowers = this.state.borrowers;
            borrowers.push(borrower);
            this.setState({ borrowers: borrowers });
        }.bind(this);

        sHub.client.logout = function() {
            this.setState({ clientRegistered: false });
        }.bind(this);

        $.connection.hub.start().done(function () {
            FB.getLoginStatus(function (response) {
                if (response.status === 'connected') {
                    console.log('Logged in.');
                    this.setState({ facebookId: response.id });
                    this._handleLogin(response.id, response.name);
                }
            }.bind(this));
        }.bind(this));

        $.connection.hub.disconnected(function () {
            setTimeout(function () {
                $.connection.hub.start().done(function () { this._getLibraryState(); }.bind(this));
            }.bind(this), 5000); // Restart connection after 5 seconds.
        });

        $.connection.hub.reconnected(function () {
            sHub.server.getLibraryState()
        }.bind(this));

        window.addEventListener("keydown", this._handleKey.bind(this), false);
    }

    componentWillUnmount() {
        window.removeEventListener("keydown", this._handleKey.bind(this), false);
        sHub.server.unregister(this.state.userName);
    }

    _getLibraryState() {
        sHub.server.getLibraryState();
    }

    _renderBooks() {
        if (this.state.bookGroups.length === 0) {
            return (<EmptyBook />);
        }

        var books = this.state.bookGroups.map(function (bookGroup) {
            if (bookGroup.length === 1) {
                return (<Book book={ bookGroup[0] }/>);
            }
            var book = bookGroup[0];
            book.Quantity = bookGroup.length;
            return (<Book book={ book }/>);
        }.bind(this));

        return books;
    }

    _renderDashboard() {
            return (
                <div>
                    <h1>{ this.state.userName }</h1>
                    { this._renderBooks() }
                    <p/>
                </div>
                );
    }

    _handleKey(event) {
        if (event.key === "s" || event.key === "S") {
            if (this.state.slideShow) {
                this._handleStopSlideShow();
                this.setState({ slideShow: false });
            } else {
                this._handleStartSlideShow();
                this.setState({ slideShow: true });
            }
        }
    }

    _registerClient(id, userName) {
        sHub.server.login({ FacebookId: id, UserName: userName });
    }

    _unregisterClient() {
        sHub.server.logout({ FacebookId: this.state.facebookId });
        this.setState({ facebookId: -1 });
    }

    _registerBook(isbn) {
        sHub.server.findBooks({ ISBN: isbn })
            .done(function(response) {
                sHub.server.registerBook({ Book: response.Books[response.Books.length - 1] });
            });
    }

    _handleFormSubmit(userName, userSecret) {
        event.preventDefault();
        this._registerClient(userName, userSecret);
    }

    _handleLogin(id, name) {
        this.setState({ facebookId: id });

        console.log('Login:' + id + ' ' + name);
        this._registerClient(id, name);
    }

    _handleLogout() {
        console.log('Logout');
        this._unregisterClient();
    }

    _renderLoginButton() {
        return (<FacebookLoginButton fb={FB} onFacebookLogin={this._handleLogin.bind(this)} onFacebookLogout={this._handleLogout.bind(this) }/>);
    }

    render() {
        var dashboard = (<span/>);
        if (this.state.clientRegistered) {

            dashboard = ( <div><RegisterBookInput onSubmit={this._registerBook.bind(this)}/> { this._renderDashboard() } </div> );
        }
        
        return (<div>
                    
                    <div>{ dashboard }</div>
                    <div>{ this._renderLoginButton()}</div>
                </div>);
        
    }
};

React.render(<Dashboard />, document.getElementById("container"));