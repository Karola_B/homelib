﻿class RegisterBookInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {isbn: ""}
    }
    
    _handleuserIsbnChange(event) {
        this.setState({ isbn: event.target.value });
    }

    render () {
        return (
            <div>
                <input id="userSecretInput" className="form-control" value={this.state.isbn} onChange={this._handleuserIsbnChange.bind(this)} />
                <button type="submit" className="btn btn-primary" onClick={ () => this.props.onSubmit(this.state.isbn) }>Register</button>
            </div>
        );
    }
};