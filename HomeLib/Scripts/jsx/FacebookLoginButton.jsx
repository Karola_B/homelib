﻿class FacebookLoginButton extends React.Component {
    constructor(props) {
      super(props);

      this.FB = props.fb;
   }

   componentDidMount() {
      this.FB.Event.subscribe('auth.logout',
         this.onLogout.bind(this));
      this.FB.Event.subscribe('auth.statusChange',
         this.onStatusChange.bind(this));
   }

   onStatusChange(response) {
      console.log( response );
      var self = this;

      if (response.status === "connected") {
          this.FB.api('/me', function(response) {
              self.props.onFacebookLogin(response.id, response.name);
          })
      } 
   }

   onLogout(response) {
       this.props.onFacebookLogout();
   }

   render() {
      return (
         <div>
            <div className="fb-login-button"
                 data-max-rows="1"
                 data-size="xlarge"
                 data-show-faces="false"
                 data-auto-logout-link="true">
            </div>
         </div>
      );
   }
};