﻿class LoginForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userName: "",
            userSecret: "",
            detailsFormValid: this.props.isValid
        };
    }

    componentDidMount () {
        React.findDOMNode(this.refs.userNameInput).focus();
    }

    _handleuserNameChange(event) {

        this.props.isValid = true;
        this.setState({ userName: event.target.value, detailsFormValid: true });
    }

    _handleuserSecretChange(event) {

        this.props.isValid = true;

        this.setState({ userSecret: event.target.value, detailsFormValid: true });
    }

    _renderValidationErrorMessage() {
        if (this.props.isValid) {
            return (<span />);
        } else {
            return (<div className="alert alert-danger" role="alert">Nie jesteśmy pewni, czy użytkownik <b>{this.state.userName}</b> ma takie hasło, o ile istnieje...</div>);
        }
    }

    render() {
        return (
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h4 className="modal-title">Witaj w Wypożyczalni Morisków!</h4>
                        </div>
                        <form>
                            <div className="modal-body">{ this._renderValidationErrorMessage() }
                                <div className="input-group">
                                    <label htmlFor="userNameInput">Nazwa użytkownika</label>
                                    <input ref="userNameInput" id="userNameInput" type="text" className="form-control" value={this.state.userName} name="Team name" onChange={this._handleuserNameChange.bind(this)} />
                                </div>
                                <br />
                                <div className="input-group">
                                    <label htmlFor="userSecretInput">Hasło</label>
                                    <input id="userSecretInput" type="userName" className="form-control" value={this.state.userSecret} onChange={this._handleuserSecretChange.bind(this)} />
                                </div>
                                <br />
                                <div className="modal-footer">
                                    <button type="submit" className="btn btn-primary" onClick={ () => this.props.onSubmit(this.state.userName, this.state.userSecret) } >Wchodzę!</button>
                                    <div class="fb-facebookId-button" data-max-rows="1" data-size="medium" data-show-faces="false" data-auto-logout-link="false"></div>
                                </div>
                                
                            </div>
                        </form>
                    </div>
                </div>);
    }
};