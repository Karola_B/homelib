﻿class Members extends React.Component {
    constructor(props) {
        super(props);
    }
    render () {
        return (<div className="container members"><div className="row">{ this.props.members.map(function(member) { return (<Member member={member}/>); }) }</div></div>); 
    }
}