﻿class Call extends React.Component {
    constructor(props) {
        super(props);
    }

    _renderCallers () {
        if (this.props.calls.length === 0) {
            return <span />
        }

        if (this.props.calls.length === 1) {
            return (<div className="single-caller"><h1>Caller: {this.props.calls[0].CallerName }</h1></div>);
        }

        return (<h1>Callers:<ul>{ this.props.calls.map(function (call) { return (<li> { call.CallerName }</li>);})}</ul></h1>);
    }

    _renderTeamCallers() {
        if (this.props.teamCalls.length === 0) {
            return (<span />);
        }

        if (this.props.teamCalls.length === 1) {
            return (<div className="single-caller"><h1>Team: {this.props.teamCalls[0].TeamName }</h1></div>);
        }

        return (<h1>Teams:<ul>{ this.props.teamCalls.map(function (call) { return (<li> { call.TeamName} </li>); })}</ul></h1>);
    }

    render () {
        return (
            <div className='call-view'>
                <div className="call-view-title"><h1>Call</h1></div>{ this._renderCallers() }{ this._renderTeamCallers() }
            </div>
        );
    }
};