﻿using System.Configuration;
using Autofac.Core;

namespace HomeLibrary
{
    public class LibraryConfiguration
    {
        public static string FacebookAppId => ConfigurationManager.AppSettings["FacebookAppId"];

        public static string FacebookAppSecret => ConfigurationManager.AppSettings["FacebookAppSecret"];
        public static string GoogleApiKey => ConfigurationManager.AppSettings["GoogleApiKey"];
        public static string GoogleApplicationName => ConfigurationManager.AppSettings["GoogleApplicationName"];
    }
}