﻿using System.Configuration;
using System.Reflection;
using Autofac;
using Autofac.Integration.SignalR;
using Autofac.Integration.WebApi;
using HomeLib.Api.Hubs;
using HomeLib.BooksInformationService;
using HomeLib.Services;
using HomeLibrary.Data;
using HomeLibrary.Data.Repositories;
using Newtonsoft.Json;

namespace HomeLibrary
{
    public class InversionOfControlConfiguration
    {
        private static IContainer _container;

        public static IContainer Container
        {
            get
            {
                if (_container == null)
                {
                    _container = BuildContainer();
                }

                return _container;
            }
        }

        private static IContainer BuildContainer()
        {
            var builder = new ContainerBuilder();

            var connectionString = ConfigurationManager.ConnectionStrings["Database"].ConnectionString;

            
            builder.RegisterType<LibraryContext>().AsSelf().InstancePerLifetimeScope();
            builder.RegisterType<BooksDbRepository>().As<IBooksRepository>();
            builder.RegisterType<UsersRepository>().As<IUsersRepository>();
            builder.RegisterType<BookService>().As<IBookService>();
            builder.RegisterType<ValidationService>().As<IValidationService>();
            builder.RegisterType<UsersService>().As<IUsersService>();
            builder.RegisterType<AuthenticationService>().As<IAuthenticationService>();
            builder.RegisterAssemblyTypes(typeof(KsiazkiOrgInformationService).Assembly)
                .Where(x => x.Name.EndsWith("InformationService"))
                .AsImplementedInterfaces()
                .WithParameters(new []
                {
                    new NamedParameter("apiKey", LibraryConfiguration.GoogleApiKey),
                    new NamedParameter("applicationName",  LibraryConfiguration.GoogleApplicationName)
                });

            builder.RegisterType<AuthorizeConnectionAttribute>()
                .PropertiesAutowired();

            builder.RegisterHubs(typeof(BooksHub).Assembly);

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            builder.RegisterAssemblyModules(Assembly.GetExecutingAssembly());
            var serializer = JsonSerializer.CreateDefault();
            builder.Register(s => serializer).As<JsonSerializer>();
            
            return builder.Build();
        }
    }
}