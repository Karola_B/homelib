using Autofac.Integration.SignalR;
using HomeLib.Api.Hubs;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Owin;

[assembly: OwinStartup(typeof(Server.Startup))]
namespace Server
{
    public class Startup
    {
        public void Configuration(IAppBuilder application)
        {
            application.UseCors(CorsOptions.AllowAll);
            var hubConfiguration = new HubConfiguration
            {
                Resolver = new AutofacDependencyResolver(InversionOfControlConfiguration.Container),
                EnableJavaScriptProxies = true,
                EnableDetailedErrors = true,
                EnableJSONP = true
            };

            var auth = hubConfiguration.Resolver.Resolve<AuthorizeConnectionAttribute>();

            GlobalHost.HubPipeline.AddModule(new AuthorizeModule(null, auth));

            application.MapSignalR("/signalr",hubConfiguration);
        }
    }
}