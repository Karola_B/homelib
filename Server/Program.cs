﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using Microsoft.Owin.Hosting;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            var startOptions = new StartOptions
            {
                Port = 8088,
                ServerFactory = "Microsoft.Owin.Host.HttpListener"
            };
            startOptions.Urls.Add($"http://{LocalIpAddress().MapToIPv4()}:8088");
            //startOptions.Urls.Add($"http://localhost:8088");
            using (WebApp.Start(startOptions))
            {
                Console.WriteLine($"Server is running on:");
                foreach (var url in startOptions.Urls)
                {
                    Console.WriteLine(url);
                }
                Console.ReadLine();
            }
        }

        private static IPAddress LocalIpAddress()
        {
            if (!System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                return null;
            }

            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());

            return host
                .AddressList
                .FirstOrDefault(ip => ip.AddressFamily == AddressFamily.InterNetwork);
        }
    }
}
