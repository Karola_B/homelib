using System.Data.Entity;
using System.Linq;
using HomeLibrary.Data.Models;

namespace HomeLibrary.Data.Repositories
{
    public class UsersRepository : IUsersRepository
    {
        private readonly LibraryContext _context;

        public UsersRepository(LibraryContext context)
        {
            _context = context;
        }

        public void AddUser(User user)
        {
            _context.Users.Add(user);
            _context.SaveChanges();
        }

        public User GetUserByFacebookId(string facebookId)
        {
            return _context.Users.FirstOrDefault(x => x.FacebookId == facebookId);
        }

        public void SaveUser(User user)
        {
            _context.Entry(user).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public User GetUserByConnection(string connectionId)
        {
            return _context.Users.FirstOrDefault(x => x.ConnectionId == connectionId);
        }
    }
}