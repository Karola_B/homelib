﻿using System.Collections.Generic;
using System.Linq;
using HomeLibrary.Data.Models;

namespace HomeLibrary.Data.Repositories
{
    public class BooksRepository : IBooksRepository
    {
        private static readonly List<Book> Books = new List<Book>();

        public IList<Book> GetBooksForLocalisation(LocalisationEnum localisation)
        {
            if (localisation == LocalisationEnum.All)
            {
                return Books.ToList();
            }

            return Books.Where(x => x.Localisation == localisation).ToList();
        }

        public Book FindBookByIsbn(string isbn)
        {
            return Books.FirstOrDefault(x => x.ISBN == isbn);
        }

        public int AddBookToLibrary(Book book)
        {
            if (Books.Any())
            {
                book.BookId = Books.Max(x => x.BookId) + 1;
            }
            else
            {
                book.BookId = 1;
            }

            Books.Add(book);

            return book.BookId;
        }

        public IList<Book> GetBooksByIsbn(string isbn)
        {
            if (Books.Any())
            {
                return Books.Where(x => x.ISBN == isbn).ToList();
            }

            return new List<Book>();
        }

        public void UpdateBook(Book existingBook)
        {
            var first = Books.First(x => x.BookId == existingBook.BookId);

            Books.Remove(first);

            Books.Add(existingBook);
        }

        public Book GetBookById(int bookId)
        {
            return Books.FirstOrDefault(x => x.BookId == bookId);
        }
    }
}