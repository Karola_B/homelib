﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using HomeLibrary.Data.Models;

namespace HomeLibrary.Data.Repositories
{
    public class BooksDbRepository : IBooksRepository
    {
        private readonly LibraryContext _context;

        public BooksDbRepository(LibraryContext context)
        {
            _context = context;
        }

        public IList<Book> GetBooksForLocalisation(LocalisationEnum localisation)
        {
            try
            {
                if (localisation == LocalisationEnum.All)
                {
                    return _context.Books.ToList();
                }

                return _context.Books.Where(x => x.Localisation == localisation).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return new List<Book>(); 
        }

        public Book FindBookByIsbn(string isbn)
        {
            return _context.Books.FirstOrDefault(x => x.ISBN == isbn);
        }

        public int AddBookToLibrary(Book book)
        {
            var addedBook = _context.Books.Add(book);
            _context.SaveChanges();
            return addedBook.BookId;
        }

        public IList<Book> GetBooksByIsbn(string isbn)
        {
            try
            {
                return _context.Books.Where(x => x.ISBN == isbn).ToList();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                return new List<Book>();
            }
        }

        public void UpdateBook(Book existingBook)
        {
            _context.Entry(existingBook).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public Book GetBookById(int bookId)
        {
            return _context.Books.Find(bookId);
        }
    }
}