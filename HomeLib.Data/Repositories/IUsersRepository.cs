﻿using HomeLibrary.Data.Models;

namespace HomeLibrary.Data.Repositories
{
    public interface IUsersRepository
    {
        void AddUser(User user);
        User GetUserByFacebookId(string facebookId);
        void SaveUser(User user);
        User GetUserByConnection(string connectionId);
    }
}