﻿using System.Collections.Generic;
using HomeLibrary.Data.Models;

namespace HomeLibrary.Data.Repositories
{
    public interface IBooksRepository
    {
        IList<Book> GetBooksForLocalisation(LocalisationEnum localisation);

        Book FindBookByIsbn(string isbn);

        int AddBookToLibrary(Book book);
        IList<Book> GetBooksByIsbn(string isbn);
        void UpdateBook(Book existingBook);
        Book GetBookById(int bookId);
    }
}