using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace HomeLibrary.Data.Models
{
    public class EncryptionService
    {
        public static string DecryptString(string encryptedString)
        {
            if (string.IsNullOrEmpty(encryptedString))
            {
                return string.Empty;
            }

            try
            {
                using (var cypher = new TripleDESCryptoServiceProvider())
                {
                    var pdb = new PasswordDeriveBytes("3be50d8413098cbaa23d1b33039b2dc8", new byte[0]);
                    cypher.Key = pdb.GetBytes(16);
                    cypher.IV = pdb.GetBytes(8);

                    using (var ms = new MemoryStream())
                    {
                        using (var cs = new CryptoStream(ms, cypher.CreateDecryptor(), CryptoStreamMode.Write))
                        {
                            var data = Convert.FromBase64String(encryptedString);
                            cs.Write(data, 0, data.Length);
                            cs.Close();

                            return Encoding.Unicode.GetString(ms.ToArray());
                        }
                    }
                }
            }
            catch
            {
                return string.Empty;
            }
        }

        public static string EncryptString(string decryptedString)
        {
            if (string.IsNullOrEmpty(decryptedString)) return string.Empty;

            using (var cypher = new TripleDESCryptoServiceProvider())
            {
                var pdb = new PasswordDeriveBytes("3be50d8413098cbaa23d1b33039b2dc8", new byte[0]);

                cypher.Key = pdb.GetBytes(16);
                cypher.IV = pdb.GetBytes(8);

                using (var ms = new MemoryStream())
                {
                    using (var cs = new CryptoStream(ms, cypher.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        var data = Encoding.Unicode.GetBytes(decryptedString);

                        cs.Write(data, 0, data.Length);
                        cs.Close();

                        return Convert.ToBase64String(ms.ToArray());
                    }
                }
            }
        }
    }
}