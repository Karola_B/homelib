﻿namespace HomeLibrary.Data.Models
{
    public class Book
    {
        public int BookId { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public string ISBN { get; set; }

        public LocalisationEnum Localisation { get; set; }

        public string Publisher { get; set; }
        public Borrower Borrower { get; set; }
        public string Description { get; set; }
        public string PublishDate { get; set; }
        public string Cover { get; set; }
        public ConditionEnum Condition { get; set; }
    }
}
