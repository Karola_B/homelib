﻿namespace HomeLibrary.Data.Models
{
    public class User
    {
        public int UserId { get; set; }
        public string Login { get; set; }
        public string FacebookId { get; set; }
        public string ConnectionId { get; set; }
        public Roles Role { get; set; }
    }
}
