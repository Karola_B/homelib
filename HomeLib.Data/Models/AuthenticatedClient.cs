﻿namespace HomeLibrary.Data.Models
{
    public class AuthenticatedClient
    {
        public virtual int Id { get; set; }

        public virtual string ConnectionId
        {
            get { return EncryptionService.DecryptString(ConnectionIdEncrypted); }
            set { ConnectionIdEncrypted = EncryptionService.EncryptString(value); }
        }

        public virtual string TeamName { get; set; }
        public virtual string ConnectionIdEncrypted { get; set; }
    }
}