﻿namespace HomeLibrary.Data.Models
{
    public enum ConditionEnum
    {
        None,
        New,
        Good,
        Bad,
        VeryBad
    }
}