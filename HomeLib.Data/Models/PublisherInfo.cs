﻿using System.Collections.Generic;

namespace HomeLibrary.Data.Models
{
    public class PublisherInfo
    {
        public int PublisherInfoId { get; set; }
        public string Name { get; set; }
        public string Year { get; set; }
        public string City { get; set; }

        public ICollection<Book> Books { get; set; } 
    }
}