﻿using System.Collections.Generic;

namespace HomeLibrary.Data.Models
{
    public class Borrower
    {
        public int BorrowerId { get; set; }
        public string Name { get; set; }

        public ICollection<Book> BorrowedBooks { get; set; } 
    }
}