﻿using System.Configuration;
using System.Data.Entity;
using HomeLibrary.Data.Models;

namespace HomeLibrary.Data
{
    public class LibraryContext: DbContext
    {
        public LibraryContext() : base("Database")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<LibraryContext, Migrations.Configuration>());
        }

        public DbSet<Book> Books { get; set; }
        public DbSet<Borrower> Borrowers { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}