﻿using Microsoft.Owin.Hosting;

namespace HomeLibrary.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            string url = "http://fp-pc2249.fp.lan:8089";

            using (WebApp.Start(url))
            {
                System.Console.WriteLine($"Server running on {url}");
                System.Console.ReadLine();
            }
        }
    }
}
