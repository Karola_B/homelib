﻿using Microsoft.Owin.Cors;
using Owin;

namespace HomeLibrary.Console
{
    class Startup
    {
        public void Configuration(IAppBuilder application)
        {
            application.UseCors(CorsOptions.AllowAll);
            application.MapSignalr();
        }
    }
}