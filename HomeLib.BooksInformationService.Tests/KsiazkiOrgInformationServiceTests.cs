﻿using NUnit.Framework;

namespace HomeLib.BooksInformationService.Tests
{
    public class KsiazkiOrgInformationServiceTests
    {
        public class WhenGivenIsbn
        {
            private BookInformation _bookInformation;

            [OneTimeSetUp]
            public void Setup()
            {
                var service = new KsiazkiOrgInformationService("","");
                _bookInformation = service.GetByIsbn("9788378397649");
            }

            [Test]
            public void ShouldFindTitleForBook()
            {
                Assert.That(_bookInformation.Title, Is.EqualTo("Cień Endera"));
            }

            [Test]
            public void ShouldFindAuthorForBook()
            {
                Assert.That(_bookInformation.Author, Is.EqualTo("Card Orson Scott"));
            }

            [Test]
            public void ShouldFindPublisherForBook()
            {
                Assert.That(_bookInformation.Publisher, Is.EqualTo("PRÓSZYŃSKI MEDIA"));
            }

            [Test]
            public void ShouldFindPublishDateForBook()
            {
                Assert.That(_bookInformation.PublishedDate, Is.EqualTo("2014"));
            }

            [Test]
            public void ShouldFindDescriptionForBook()
            {
                Assert.That(_bookInformation.Description, Is.Not.Null);
            }

            [Test]
            public void ShouldFindCoverImageForBook()
            {
                Assert.That(_bookInformation.Cover, Is.Not.Null);
            }
        }
    }
}
